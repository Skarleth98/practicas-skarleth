package prueba.skarlethmendoza.facci.primeraapp4b;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {

    EditText editTextIngresarUsuario,editTextIngresarClave;
    Button buttonAutenticar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        editTextIngresarUsuario = findViewById(R.id.editTextUsuario);
        editTextIngresarClave= findViewById(R.id.editTextClave);
        buttonAutenticar = findViewById(R.id.autenticar);
        buttonAutenticar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(
                        Main2Activity.this, MainActivity.class);

                startActivity(intent);

            }
        });
        editTextIngresarUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(
                        Main2Activity.this,MainActivity.class);

                startActivity(intent);

            }
        });
        editTextIngresarClave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(
                        Main2Activity.this, MainActivity.class);

                startActivity(intent);

            }
        });
    }
}
