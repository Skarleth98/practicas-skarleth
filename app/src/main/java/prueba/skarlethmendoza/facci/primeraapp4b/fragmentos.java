package prueba.skarlethmendoza.facci.primeraapp4b;

import android.app.FragmentTransaction;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class fragmentos extends AppCompatActivity implements View.OnClickListener, FrgUno.OnFragmentInteractionListener, FrgDos.OnFragmentInteractionListener{

    Button botonFrgUno, botonFrgDos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragmentos);

        botonFrgUno = (Button) findViewById(R.id.buttonFragmentoUno);
        botonFrgDos = (Button) findViewById(R.id.buttonFragmentoDos);

        botonFrgUno.setOnClickListener(this);
        botonFrgDos.setOnClickListener(this);

    }


    public void onClick(View v){
        switch (v.getId()) {

            case R.id.buttonFragmentoUno:
            FrgUno frgmentoUno = new FrgUno();
            android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.contenedor, frgmentoUno);
            transaction.commit();
            break;

        }
        switch (v.getId()) {

            case R.id.buttonFragmentoDos:
                FrgDos frgmentoDos = new FrgDos();
                android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.contenedor, frgmentoDos);
                transaction.commit();
                break;
        }
    }
    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
