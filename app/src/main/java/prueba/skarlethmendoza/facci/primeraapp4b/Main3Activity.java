package prueba.skarlethmendoza.facci.primeraapp4b;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Main3Activity extends AppCompatActivity {

    Button buttonIngresarDatos;
    EditText editTextIngresarNombre, editTextIngresarApellidos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        buttonIngresarDatos = findViewById(R.id.button);
        editTextIngresarNombre= findViewById(R.id.editText3);
        editTextIngresarApellidos = findViewById(R.id.editText4);



        buttonIngresarDatos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(
                        Main3Activity.this, MainActivity.class);

                startActivity(intent);

            }
        });
        editTextIngresarNombre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(
                        Main3Activity.this, Main3Activity.class);

                startActivity(intent);

            }
        });
        editTextIngresarApellidos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(
                        Main3Activity.this, Main3Activity.class);

                startActivity(intent);

            }
        });
    }
}
