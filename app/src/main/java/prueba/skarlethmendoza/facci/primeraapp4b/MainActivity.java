package prueba.skarlethmendoza.facci.primeraapp4b;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button buttonLogin, buttonGuardar, buttonBuscar, buttonPasarParametro, buttonFragmento, buttonAutenticar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonLogin = findViewById(R.id.buttonLogin);
        buttonGuardar= findViewById(R.id.buttonGuardar);
        buttonBuscar = findViewById(R.id.buttonBuscar);
        buttonPasarParametro = findViewById(R.id.buttonpasarparametro);
        buttonFragmento = findViewById(R.id.buttonfragmento);

        buttonPasarParametro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(
                        MainActivity.this, ActividadPasarParametro.class);

                startActivity(intent);

            }
        });
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(
                        MainActivity.this, Main2Activity.class);

                startActivity(intent);

            }
        });
        buttonGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(
                        MainActivity.this, Main3Activity.class);

                startActivity(intent);

            }
        });

        buttonBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(
                        MainActivity.this, Main4Activity.class);

                startActivity(intent);

            }
        });
        buttonFragmento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(
                        MainActivity.this, fragmentos.class);

                startActivity(intent);

            }
        });


    }


    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){

        switch (item.getItemId()) {
            case R.id.opcionLogin:
                Dialog dialogLogin = new Dialog(MainActivity.this);
                dialogLogin.setContentView(R.layout.dig_log);

               Button buttonAutenticar = (Button) dialogLogin.findViewById(R.id.buttonAut);
               final EditText cajaUsuario = (EditText) dialogLogin.findViewById(R.id.editTextUsuario);
                final EditText cajaClave = (EditText) dialogLogin.findViewById(R.id.editTextClave);

                buttonAutenticar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(MainActivity.this,cajaUsuario.getText().toString() + " " + cajaClave.getText().toString(), Toast.LENGTH_SHORT);
                    }
                });



                dialogLogin.show();
                break;

        }
        switch (item.getItemId()) {
            case R.id.opcionRegistrar:

                break;
        }
        return true;
    }

}
