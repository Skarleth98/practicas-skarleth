package prueba.skarlethmendoza.facci.primeraapp4b;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Main4Activity extends AppCompatActivity {
    EditText editTextIngreseCedula;
    Button buttonBuscar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main4);

        editTextIngreseCedula= findViewById(R.id.editText);
        buttonBuscar = findViewById(R.id.button2);
        buttonBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(
                        Main4Activity.this, MainActivity.class);

                startActivity(intent);

            }
        });
        editTextIngreseCedula.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(
                        Main4Activity.this, Main4Activity.class);

                startActivity(intent);

            }
        });
    }
}
